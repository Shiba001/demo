
import java.awt.Graphics;
import java.io.File;
import java.util.ArrayList;

import javax.swing.filechooser.FileSystemView;



public class AviApplet extends javax.swing.JApplet {
 
 ArrayList<String> str=new ArrayList<String>();
 @Override
 public void stop(){
  
 }
 @Override
 public void start(){
  
 }
 @Override
 public void init(){
  //str=pplet programming";
  
   FileSystemView fsv = FileSystemView.getFileSystemView();
         
         File[] drives = File.listRoots();
         if (drives != null && drives.length > 0) {
             for (File aDrive : drives) {
              str.add("Drive Letter: " + aDrive);
                 System.out.println("\tType: " + fsv.getSystemTypeDescription(aDrive));
                 System.out.println("\tTotal space: " + aDrive.getTotalSpace());
                 System.out.println("\tFree space: " + aDrive.getFreeSpace());
                 System.out.println();
             }
         }
     }
  
 
 public void paint(Graphics g){
  /*g.setColor(Color.white);
  g.fillRect(0, 0, 400, 400);*/
  int i=10;
  for(String s:str){
  
   g.drawString(s, i, i);
   i+=10;
  }
 }
 
}