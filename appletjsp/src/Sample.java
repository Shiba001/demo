
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;


public class Sample {
    //keystore related constants
    private static String keyStoreFile = "/home/hasini/Digital-Signature/sample/src/main/resources/mykeystore.jks";
    private static String password = "mypassword";
    private static String alias = "mycert";

    public static void main(String[] args) {

        try {
            KeyStore keystore = KeyStore.getInstance("JKS");
            char[] storePass = password.toCharArray();

            //load the key store from file system
            FileInputStream fileInputStream = new FileInputStream(keyStoreFile);
            keystore.load(fileInputStream, storePass);
            fileInputStream.close();

            /***************************signing********************************/
            //read the private key
            KeyStore.ProtectionParameter keyPass = new KeyStore.PasswordProtection(storePass);
            KeyStore.PrivateKeyEntry privKeyEntry = (KeyStore.PrivateKeyEntry) keystore.getEntry(alias, keyPass);
            PrivateKey privateKey = privKeyEntry.getPrivateKey();

            //initialize the signature with signature algorithm and private key
            Signature signature = Signature.getInstance("SHA256withRSA");
            signature.initSign(privateKey);

            //Read the string into a buffer
            String data = "{\n" +
                          "  \"schemas\":[\"urn:scim:schemas:core:1.0\"],\n" +
                          "  \"userName\":\"bjensen\",\n" +
                          "  \"externalId\":\"bjensen\",\n" +
                          "  \"name\":{\n" +
                          "    \"formatted\":\"Ms. Barbara J Jensen III\",\n" +
                          "    \"familyName\":\"Jensen\",\n" +
                          "    \"givenName\":\"Barbara\"\n" +
                          "  }\n" +
                          "}";

            byte[] dataInBytes = data.getBytes();

            //update signature with data to be signed
            signature.update(dataInBytes);

            //sign the data
            byte[] signedInfo = signature.sign();

            System.out.println(signedInfo.toString());

            /**************************verify the signature****************************/
            Certificate publicCert = keystore.getCertificate(alias);

            //create signature instance with signature algorithm and public cert, to verify the signature.
            Signature verifySig = Signature.getInstance("SHA256withRSA");
            verifySig.initVerify(publicCert);

            //update signature with signature data.
            verifySig.update(dataInBytes);

            //verify signature
            boolean isVerified = verifySig.verify(signedInfo);

            if (isVerified) {
                System.out.println("Signature verified successfully");
            }
            
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (UnrecoverableEntryException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();  
        }
    }
}
